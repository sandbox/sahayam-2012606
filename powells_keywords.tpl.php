<div class="powells-search-books">
	<div class="powells-search-content">
		<?php //echo "<pre>"; print_r($powells_results); echo "</pre>"; exit;
	  foreach($powells_results as $powells_book) {
			$isbn_image =  $powells_book->cover_image;
			$alt = "Book Cover";		
		?>
		<div class="powells-search-row">
			<div class="search-result-left">
				<div class="search-result-left-img">
					<img width="107" height="140" src="<?php print $isbn_image; ?>" alt="<?php print $alt; ?>" title= "<?php print $powells_book->title; ?>" >
				</div>
				<div class="search-result-left-desc">  
					<div class="field-title">
					 <span class=label">Title: </span>
						<span class="field-content">
							<a href="<?php print $powells_book->product_page; ?>" target="_blank"><?php print $powells_book->title; ?></a>
						</span>
					</div>  
					<div class="field-isbn13">
						<span class=label">ISBN13: </span>
						<span class="field-content"><?php print $powells_book->isbn13; ?></span>
					</div>  
					<div class="field-author">
					 <span class=label">Author: </span>
						<span class="field-content">
							<?php print $powells_book->author; ?>
						</span>
					</div>  
					<div class="field-publisher">
						<?php if($powells_book->publisher != "") { ?>
							<span class=label">Publisher: </span>
							<span class="field-content"><?php print $powells_book->publisher; ?></span>
						<?php } ?>
					</div>					
				</div>
			</div>
    </div>
		<?php } ?>
		<?php 
		if(empty($powells_results)) { ?>
		<div class="error-msg-wrap">
			<div class="msg-content">
				<h3>No results match your search for "<?php print arg(1); ?>"</h3>         
				<p> Make sure that the information you are searching for is spelled correctly.</p>
			</div>        
		</div>
		<?php } ?>
	</div>
</div>

$Id: README.txt,v 10 2013/05/06 01:36:46 sahayam Exp $

Powells Search helps the user to retrieve books from Powells using Powells partnership API.
You can configure the settings under "Powells Search Settings" at 
admin/config/content/powells_search. You can enter your partner id, if you
have partner id from Powells Partner Program. Here you can modify the number of results page.
After enable this module, it will show a navigation menu "Search Books".

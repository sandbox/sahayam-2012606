<?php
// $Id: powells_search.module,v 1.0 2013/05/06 14:19:12 sahayam Exp $
/**
 * @file
 * The book search module.
 *
 * Helps to search book in Powells from the drupal website
 */


/**
 * Implementation of hook_init()
 */
function powells_search_init() {
 drupal_add_css(drupal_get_path("module", "powells_search") . "/css/powells_search.css");
}
/**
 * Implementation of hook_perm()
 */
function powells_search_permission() {
  return array(
    'administer powells_search' => array('title' => t('Administer Powells Search'),
      'description' => t('Configuration for Powells Search.'),
      'restrict access' => TRUE,
    ),
    'access powells_search' => array('title' => t('Access Powells Search'),
      'description' => t('Access Powells Search.'),
      'restrict access' => TRUE,
    ),
  );
}
/**
* Implementation of hook_menu()
*/
function powells_search_menu() {
  $items['admin/config/content/powells_search'] = array(
    'title' => 'Powells Search Settings',
    'description' => 'Configure required settings for Powells Search.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('powells_search_settings_form'),
    'access arguments' => array('administer powells_search'),
    'type' => MENU_NORMAL_ITEM
  );
  $items['powells-search/%'] = array(
     'title' => 'Book Results',
     'page callback' => 'powells_search_get_results_by_keyword',
     'page arguments' => array(1),
     'access callback' => TRUE,
     'type' => MENU_CALLBACK
  );
  $items['search-books/powells'] = array(
     'title' => 'Search Books',
     'page callback' => 'drupal_get_form',
     'page arguments' => array('powells_search_search_books_form'),
     'access arguments' => array('access powells_search'),
     'type' => MENU_NORMAL_ITEM
  );
  return $items;
}
/**
* Returns form to get the keyword from user
*/
function powells_search_search_books_form($form, &$form_state) {
  $form = array();
  $form['keyword'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter the keyword'
   );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit'
  );
  return $form;
}
/**
 * Implementation of form submit
 * It redirects the user to the results page with keyword
*/
function powells_search_search_books_form_submit(&$form, &$form_state) {
  $form_state['redirect'] =  'powells-search/' . $form_state['values']['keyword'];  
}
/**
 * Implementation of hook_settings_form()
 * It gets the partner id from powells and the api url
*/
function powells_search_settings_form() {
  $form = array();
  $form['powells_partner_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Powells Partner ID'),
    '#default_value' => variable_get('powells_partner_id', 'testing'),
    '#maxlength' => 64,
    '#description' => t("If you want to search book from Powells using their api, please provide the partnership id you have got from Powells"),
  );
  $form['use_powells_api'] = array(
    '#type' => 'radios',
    '#title' => t('Do you want to search results using Powells Partnership API?'),
    '#default_value' => variable_get('use_powells_api', '1'),
    '#options' => array(1 => 'Yes', 0 => 'No'),
    '#description' => t("If Yes, the results will be retrieved using Powells API with the partner id. Sometimes, this may not give the exact results as in powells.com"),
  );
  $form['powells_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the API url of the powells'),
    '#default_value' => variable_get('powells_api_url', 'http://api.powells.com/v0c'),
    '#maxlength' => 64,
    '#description' => t('Enter URL of the powells API' ),
  );
  $form['powells_results_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Results per page'),
    '#default_value' => variable_get('powells_results_per_page', 10),
    '#maxlength' => 64,
    '#description' => t("Enter the number of results per page to get from Powell's" ),
  );  
  return system_settings_form($form);
}
/**
 * Implementation of hook_theme()
 */
function powells_search_theme() {
  return array(
    'powells_keywords' => array(
      'template' => 'powells_keywords',
      'arguments' => array('powells_results' => NULL, 'powells_results_total' => NULL),
    ),
  );
}
/* 
 * Function to get the search results from Powells using Powells API
*/ 
function powells_search_get_results_by_keyword_api($keyword, $limit, $page) {
  $results = '';
  if ( !empty ( $keyword ) ) {
     $powells_partner_id = variable_get('powells_partner_id', 'testing');
     $powells_api_url = variable_get('powells_api_url', 'http://api.powells.com/v0c');
     $powells_results_per_page = $limit;     
     if (!empty ( $powells_partner_id ) ) {
       $api_endpoint = $powells_api_url . '/' . $powells_partner_id . '/search/' . $keyword . '/' . $page . '?per_page=' . $powells_results_per_page;
       // Load the book details
       $results = drupal_http_request($api_endpoint);
       $results = @json_decode(@$results->data);
     }     
  }
  return $results;
}
/**
 * This function is used to get the records for a keyword from powells
 * It used drupal_http_request method to get the results from url and
 * using html dom parser to parse it and format the content
*/
function powells_search_get_results_by_keyword( $keyword = '') {
  global $pager_page_array, $pager_total;
  $keyword = htmlentities(trim($keyword), ENT_QUOTES); 
  $keyword = str_replace(" ", "%20", $keyword);
  // Grab the 'page' query parameter.
  $page = isset($_GET['page']) ? $_GET['page'] : 0;
  $pager_page_array = explode(',', $page);
  $results_per_page = variable_get('powells_results_per_page', 10);
  $start = $page*$results_per_page;  
  $powells_books = array();
  $use_api = variable_get('use_powells_api', '1');
  if ($use_api == 1) {
    $powells_results = powells_search_get_results_by_keyword_api($keyword, $results_per_page, $page);
    $total_records = $powells_results->meta->page_results;
    if ($total_records > 0) { 
    foreach ($powells_results->results as $result) {
      if (@$result->title != '' && @$result->isbn != '') {
        $powells_book = new stdClass();
        $powells_book->title = @$result->title;
        $powells_book->author = @$result->author->proper;
        $powells_book->publisher = @$result->publisher;
        $powells_book->isbn13 = @$result->isbn;
        $powells_book->product_page = @$result->product_page;
        $powells_book->cover_image = @$result->cover_images->{120};
        array_push($powells_books, $powells_book);
      }
     }
    }    
  }
  else {
    module_load_include('php', 'powells_search', 'simple_html_dom');
    // URL to login page
    $url = "http://www.powells.com/s3?kw=" . $keyword . "&perpage=$results_per_page&start=$start";
    $result = drupal_http_request($url);
    $output = $result->data;
    $powells_books = array();
    $html = str_get_html($output);
    if (is_object($html)) {
      $book_content = $html->find('td[id=content] ol.booklist', 0);
      if (is_object($book_content)) { 
        foreach ($book_content->find('li') as $element) {
          if (is_object($element)) {
            $powells_book = new stdClass();
            $title =  @$element->find('h3 a', 0)->innertext;
            $product_page = @$element->find('h3 a', 0)->href;
            $author = @$element->find('div.book-info div cite', 0)->plaintext;
            $author_publisher = @$element->find('div.book-info div', 0)->plaintext;
            $publisher = @str_replace($author . '&nbsp;/&nbsp;', '', $author_publisher);
            $isbn = @str_replace("p-", '', $element->find('p.also-available', 0)->id);
            $cover_image = @$element->find('a img', 0)->src;
            $powells_book->title = $title;
            $powells_book->author = $author;
            $powells_book->publisher = $publisher;
            $powells_book->product_page = $product_page;
            $powells_book->isbn13 = $isbn;
            $powells_book->cover_image = $cover_image;
            if ($title != '' && @$isbn != '') {
              array_push($powells_books, $powells_book);
            }
          }
        }
      }
    }
    $total_records = @($html->find('div[id=pagination] b', 1)->innertext);
    $total_records = (int)(str_replace(",", '', $total_records));
  }
  $pager_total[0] = ceil($total_records/$results_per_page);
  $output =  theme('powells_keywords', array('powells_results' => $powells_books, 'powells_total' => $total_records));
  $output .= theme('pager');
  return $output;  
}
